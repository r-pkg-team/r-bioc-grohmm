r-bioc-grohmm (1.40.3-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 21:17:46 +0100

r-bioc-grohmm (1.40.3-1) experimental; urgency=medium

  * Team upload.
  * New upstream version

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 27 Dec 2024 12:12:46 -0800

r-bioc-grohmm (1.38.0-2) experimental; urgency=medium

  * Team upload.
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 29 Nov 2024 13:52:50 +0100

r-bioc-grohmm (1.38.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 12:09:14 +0200

r-bioc-grohmm (1.38.0-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 07 Aug 2024 16:21:03 +0200

r-bioc-grohmm (1.38.0-1~0exp) experimental; urgency=low

  * Team upload.
  * New upstream version
  * Set upstream metadata fields: Archive.
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 17:23:35 +0200

r-bioc-grohmm (1.36.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Add d/t/autopkgtest-pkg-r.conf

 -- Andreas Tille <tille@debian.org>  Fri, 01 Dec 2023 07:50:13 +0100

r-bioc-grohmm (1.34.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jul 2023 06:40:24 +0200

r-bioc-grohmm (1.32.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 18:26:11 +0100

r-bioc-grohmm (1.30.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Drop S.h patch now applied upstream

 -- Andreas Tille <tille@debian.org>  Thu, 19 May 2022 15:27:46 +0200

r-bioc-grohmm (1.29.1-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

  [ Nilesh Patra ]
  * Remove S.h includes

 -- Nilesh Patra <nilesh@debian.org>  Mon, 16 May 2022 18:57:16 +0530

r-bioc-grohmm (1.28.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2021 22:10:44 +0100

r-bioc-grohmm (1.26.0-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on r-bioc-genomicalignments,
      r-bioc-genomicranges, r-bioc-iranges, r-bioc-rtracklayer and
      r-bioc-s4vectors.

  [ Nilesh Patra ]
  * d/control: Versioned B-D as per DESCRIPTION file

 -- Nilesh Patra <nilesh@debian.org>  Sat, 11 Sep 2021 23:37:20 +0530

r-bioc-grohmm (1.24.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 04 Nov 2020 11:29:22 +0100

r-bioc-grohmm (1.22.0-2) unstable; urgency=medium

  * Team upload.
  * debhelper-compat 13 (routine-update)
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Thu, 18 Jun 2020 08:17:31 +0200

r-bioc-grohmm (1.22.0-1) unstable; urgency=medium

  * Initial release (closes: #962661)

 -- Steffen Moeller <moeller@debian.org>  Tue, 09 Jun 2020 02:03:19 +0200
